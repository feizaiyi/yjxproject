from django.apps import AppConfig


class VerivicationConfig(AppConfig):
    name = 'verification'
