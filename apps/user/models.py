from django.db import models
from django.contrib.auth.models import AbstractUser, UserManager as _UserManager
# from course.models import CourseCategory


class UserManager(_UserManager):
    """
    自定义 user manager 修改在使用`python manage.py createsuperuser`命令时
    可以提供email
    """
    def create_superuser(self, username, password, email=None, **extra_fields):
        return super().create_superuser(username=username, password=password, email=email, **extra_fields)


class User(AbstractUser):
    """
    add mobile, email_active fields to Django user model.
    """
    mobile = models.CharField('手机号', max_length=11, unique=True, help_text='手机号', error_messages={'unique': '此手机号码已注册'})
    name = models.CharField("姓名", max_length=56, help_text="姓名")
    school = models.CharField("学校", max_length=56, help_text="学校")
    job = models.CharField("职位", max_length=56, help_text="职位")
    # sex = models.IntegerField("性别", help_text="性别")
    email_active = models.BooleanField('邮箱状态', default=False)
    w = models.ManyToManyField('course.CourseCategory',null=True)

    class Meta:
        db_table = 'tb_user'    # 指定数据库表名
        verbose_name = '用户'     # 在admin站点中显示名称
        verbose_name_plural = verbose_name  # 显示复数

    def __str__(self):
        return self.username

    # A list of the field names that will be prompted for
    # when create a user via createsuperuser management command.
    REQUIRED_FIELDS = ['mobile']
    # specify manager
    objects = UserManager()
