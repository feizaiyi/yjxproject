import logging
import random

from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from django.views import View
from django_redis import get_redis_connection

from utils.captcha.captcha import captcha
from . import constants
from user.models import User
from utils.res_code import json_response, Code, error_map
# from .forms import CheckImagForm
# from utils.yuntongxun.sms import CCP

# 日志器
logger = logging.getLogger('django')


def img_code_view(request):
    """
    生成验证码
    url: /image_code/
    :param request:
    :return:
    """
    # 1. 生成一个验证码，随机生成字符串，生成图片
    text, image = captcha.generate_captcha()
    # 2. 在后端保存验证码，为了等下他来校验
    # 保存在session中
    request.session['image_code'] = text
    # 给个过期时间,设置session的过期时间
    request.session.set_expiry(constants.IMAGE_CODE_EXPIRES)
    # 3. 记录一个日志
    logger.info('Image code:{}'.format(text))
    # 4. 返回验证码图片
    return HttpResponse(content=image, content_type='image/jpg')


def check_username_view(request, username):
    """
    校验用户名
    url:/username/(?P<username>\w{5,20})/
    :param request:
    :param username:
    :return:
    """
    # 去数据库查一下，然后返回就完了
    data = {
        "errno": Code.OK,
        "errmsg": "OK",
        "data": {
            "username": username,  # 查询用户名
            "count": User.objects.filter(username=username).count()  # 用户查询数量
        }
    }
    data = {
        "username": username,  # 查询用户名
        "count": User.objects.filter(username=username).count()  # 用户查询数量
    }
    # 返回json数据

    return json_response(data=data)


def check_mobile_view(request, mobile):
    """
    校验手机号码
    url:/mobile/(?P<mobile>1[3-9]\d{9})/
    :param request:
    :param mobile:
    :return:
    """
    # 去数据库查一下，然后返回就完了
    data = {
        "errno": "0",
        "errmsg": "OK",
        "data": {
            "mobile": mobile,  # 查询用户名
            "count": User.objects.filter(mobile=mobile).count()  # 用户查询数量
        }
    }
    # 返回json数据
    data = {
        "mobile": mobile,  # 查询用户名
        "count": User.objects.filter(mobile=mobile).count()  # 用户查询数量
    }

    return json_response(data=data)


# class SmsCodeView(View):
#     """
#     发送短信验证码
#     url: /sms_code/
#     """
#     def post(self, request):
#         """
#
#         -
#         - 发送短信
#         - 保存这个短信验证码（保存在哪里？）
#         - 保存发送记录
#         """
#         form = CheckImagForm(request.POST, request=request)
#         if form.is_valid():
#             # 获取手机号码
#             mobile = form.cleaned_data.get('mobile')
#             # 生成短信验证码
#             sms_code = ''.join([random.choice('0123456789') for _ in range(constants.SMS_CODE_LENGTH)])
#             # 发送短信验证码 调用接口
#             ccp = CCP()
#             try:
#                 res = ccp.send_template_sms(mobile, [sms_code, constants.SMS_CODE_EXPIRES], "1")
#                 if res == 0:
#                     logger.info('发送短信验证码[正常][mobile: %s sms_code: %s]' % (mobile, sms_code))
#                 else:
#                     logger.error('发送短信验证码[失败][moblie: %s sms_code: %s]' % (mobile, sms_code))
#                     return json_response(errno=Code.SMSFAIL, errmsg=error_map[Code.SMSFAIL])
#             except Exception as e:
#                 logger.error('发送短信验证码[异常][mobile: %s message: %s]' % (mobile, e))
#                 return json_response(errno=Code.SMSERROR, errmsg=error_map[Code.SMSERROR])
#
#             # 保存这个验证码 时限redis
#             # 创建短信验证码发送记录的key
#             sms_flag_key = 'sms_flag_{}'.format(mobile)
#             # 创建短信验证码内容的key
#             sms_text_key = 'sms_text_{}'.format(mobile)
#
#             redis_conn = get_redis_connection(alias='verify_code')
#             pl = redis_conn.pipeline()
#
#             try:
#                 pl.setex(sms_flag_key, constants.SMS_CODE_INTERVAL, 1)
#                 pl.setex(sms_text_key, constants.SMS_CODE_EXPIRES*60, sms_code)
#                 # 让管道通知redis执行命令
#                 pl.execute()
#                 return json_response(errmsg='短信验证码发送成功！')
#             except Exception as e:
#                 logger.error('redis 执行异常：{}'.format(e))
#
#                 return json_response(errno=Code.UNKOWNERR, errmsg=error_map[Code.UNKOWNERR])
#
#         else:
#             # 将表单的报错信息进行拼接
#             err_msg_list = []
#             for item in form.errors.values():
#                 err_msg_list.append(item[0])
#
#             err_msg_str = '/'.join(err_msg_list)
#             return json_response(errno=Code.PARAMERR, errmsg=err_msg_str)