from django.urls import path
from . import views

app_name = "myadmin"

urlpatterns = [
    path('', views.IndexView.as_view(), name="index"),
    path('home/', views.HomeView.as_view(), name='home'),
    path('wait/', views.WaitView.as_view(), name='wait'),
    path('menus/', views.MenuListView.as_view(), name='menu_list'),
    path('menu/', views.MenuAddView.as_view(), name='menu_add'),
    path('menu/<int:menu_id>/', views.MenuUpdateView.as_view(), name='menu_manage'),
    path("users/", views.UserListView.as_view(),name="user_list"),
    path('user/<int:user_id>/', views.UserUpdateView.as_view(), name='user_update'),
    path('groups/', views.GroupListView.as_view(), name="group_list"),
    path('group/', views.GroupAddView.as_view(), name='group_add'),
    path('group/<int:group_id>/', views.GroupUpdateView.as_view(), name='group_update'),
    path('group_del/<int:id>/', views.GroupDelView.as_view(), name='group_del'),
    path('courses/', views.CourseListView.as_view(), name='course_list'),
    path('course2/<int:pk>/', views.CourseUpdateView.as_view(), name='course_update'),
    path('course/add1/', views.CourseAddView1.as_view(), name='course_lesson'),
    path('course/add/', views.CourseLessonView.as_view(), name='course_add'),
    path('course/<int:course_id>/', views.CourseAddView.as_view(), name='course_delete'),
    path('courses/index/', views.CoursesManageView.as_view(), name='course_manage'),
    path('courses1/<int:course_id>/', views.CoursesManageView.as_view(), name='course_unpass'),
    path('coursecatetypelist/',views.CategoryList.as_view(),name='cate_type_list'),
    path('coursecatetypeadd/',views.CourseCateTypeAdd.as_view(),name='cate_type_add'),
    path('coursecatetypeupdate/<int:pk>/',views.CourseCateTypeUpdate.as_view(),name='cate_type_update'),
    path('coursecatetype/<int:id>/',views.CourseCateTypeDel.as_view(),name='cate_type_del'),
    path('coursecatelist/<int:category_type_id>/',views.CourseCateList.as_view(),name='cate_list'),
    path('coursecatelist/add/',views.CourseCateAdd.as_view(),name='course_cate_add'),
    path('coursecatelistdel/<int:id>/',views.CousreCateDel.as_view(),name='course_cate_del'),
    # path('coursecatelistupdate/<int:pk>/',views.CourseCateUpdate.as_view(),name='course_cate_update'),
    path('coursecatelistupdate/<int:menu_id>/',views.CourseCateUpdate.as_view(),name='course_cate_update'),
    path('coursecatelist1/<int:category_id>/',views.CourseCateDetail.as_view(),name='course_cate_detail'),
    path('course/lesson/<int:category_id>/',views.Courselesson.as_view(),name='course_lesson'),
    path('course/lesson_detail/',views.CourseLessonDetail.as_view(),name='course_lesson_detail'),
    path('course/lesson_add/',views.CourseLessonAdd.as_view(),name='lesson_add'),
    path('lesson/lesson_list/',views.LessonList.as_view(),name='lesson_list'),
    path('lesson/<int:id>/',views.LessonList.as_view(),name='lesson_del'),
    path('lesson1/<int:pk>/', views.LessonUpdateView.as_view(), name='lesson_update'),
    path('coursebag/', views.CourseBagList.as_view(), name='course_bag'),
    path('coursebag1/', views.CourseBagAdd.as_view(), name='coursebag_add'),
    path('coursebag/<int:id>/', views.CourseBagUpdate.as_view(), name='coursebag_update'),
    path('coursebagnameadd/', views.Coursebagnameadd.as_view(), name='coursebagnameadd'),
    path('play/', views.Play.as_view(), name='play'),

]
