# Generated by Django 2.1.10 on 2020-01-06 09:43

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('course', '0011_auto_20200106_1535'),
    ]

    operations = [
        migrations.AddField(
            model_name='coursebag',
            name='m',
            field=models.ManyToManyField(to='course.Course'),
        ),
        migrations.AlterField(
            model_name='coursebag',
            name='name',
            field=models.CharField(help_text='课件包名称', max_length=100, verbose_name='课件包名称'),
        ),
    ]
