# Generated by Django 2.1.10 on 2020-01-06 01:50

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('course', '0008_auto_20200103_1816'),
    ]

    operations = [
        migrations.CreateModel(
            name='CoursePack',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('create_time', models.DateTimeField(auto_now_add=True, verbose_name='创建时间')),
                ('update_time', models.DateTimeField(auto_now=True, verbose_name='更新时间')),
                ('is_delete', models.BooleanField(default=False, verbose_name='逻辑删除')),
                ('name', models.CharField(help_text='课程章节', max_length=100, verbose_name='课程章节')),
            ],
            options={
                'verbose_name': '课件包',
                'verbose_name_plural': '课件包',
                'db_table': 'tb_course_pack',
            },
        ),
        migrations.AddField(
            model_name='course',
            name='course_pack',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='course.CoursePack'),
        ),
        migrations.AddField(
            model_name='courselesson',
            name='course_pack',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='course.CoursePack'),
        ),
    ]
