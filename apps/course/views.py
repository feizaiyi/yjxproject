from django.shortcuts import render
from django.db.models import Q
from django.http import Http404
from django.http import HttpResponse
from django.views import View
from .models import Course,CourseCategory,CourseBag,CourseLesson
from utils.res_code import json_response, Code

# Create your views here.
class IndexView(View):
    def get(self,request):
        aa=request.user
        print(aa)
        category = CourseCategory.objects.filter(user__username=aa)
        categor = CourseCategory.objects.values('parent__name','parent_id').filter(user__username=aa).distinct()
        print(categor)

        aa = CourseCategory.objects.filter(user__username=aa)

        return render(request, 'course/course1.html', context={'category': category,
                      "categor":categor})

class Coursecategory(View):
    def get(self,request,parent_id):
        aa = request.user
        category = CourseCategory.objects.filter(parent_id=parent_id,user__username=aa)
        print(category)

        return render(request,'course/coursecategory.html',context={
            'category':category
        })



class CategoryList(View):
    def get(self,request):
        # user = request.uesr
        # print(user)
        category= CourseCategory.objects.filter(~Q(parent= None))
        # print(category)
        # categorys = []
        # for obj in category:
        #     categorys.append(obj)
        #     child=obj.children.all()
        #     # if len(child)>0:
            #     categorys.append(CategoryList(child))
        print(category)


        return render(request,'course/index.html',context={
            'category':category
        })





class CourseDetailView(View):

    def get(self,request,category_id):
        courses = Course.objects.only('title', 'cover_url','video_url', 'teacher__name', 'teacher__title','category__name', 'lesson').filter(is_delete=False,category_id=category_id,is_pass=1).order_by('title')
        # course_lesson = Course.objects.values('lesson__name').filter(is_delete=False,category_id=category_id,is_pass=1).distinct()
        course_lesson = CourseLesson.objects.filter(category_id=category_id)
        print(course_lesson)


       # courses=list(courses)
        return render(request,'course/course_detail1.html',context={
            'courses':courses,
            'category':category_id,
            'course_lesson':course_lesson,

        })
    # def post(self,request,category_id):
    #     cc = request.POST.get('category_id')
    #     print(cc)
    #     if cc:
    #         courses = Course.objects.filter(category_id=cc)
    #         return render(request, 'course/course_detail1.html', context={
    #             'courses': courses,
    #             'category': cc,
    #             # 'course_lesson': course_lesson,
    #
    #         })
    #
    #     else:
    #         return json_response(errmsg="错误")

class Category_detailView(View):
    def get(self,request,category_id):
        courses =Course.objects.filter(category_id=category_id)
        return render(request,'course/content.html',context={'courses':courses})