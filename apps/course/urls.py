from django.urls import path
from .import views

app_name = "course"

urlpatterns = [
    path('', views.IndexView.as_view(), name="index"),
    path('<int:category_id>/', views.CourseDetailView.as_view(), name='course_detail'),
    path('categorys/<int:category_id>/', views.Category_detailView.as_view(), name='category_detail'),
    path('list/', views.CategoryList.as_view(), name='cate_list'),
    path('category/<int:parent_id>', views.Coursecategory.as_view(), name='category'),

]