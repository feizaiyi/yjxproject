from django.db import models

from utils.models import BaseModel
# from user.models import User


class Teacher(BaseModel):
    name = models.CharField('讲师姓名', max_length=150, help_text='讲师姓名')
    title = models.CharField('职称', max_length=150, help_text='职称')
    profile = models.TextField('简介', help_text='简介')
    photo = models.URLField('头像url', default='', help_text='头像url')

    class Meta:
        db_table = 'tb_teachers'
        verbose_name = '讲师'
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.name


class CourseCategory(BaseModel):
    name = models.CharField('课程名', max_length=100, help_text='课程名',null=True)
    title = models.CharField('课程名', max_length=150, help_text='课程名', null=True)
    category_type = models.ForeignKey('CourseCategoryType',help_text='课程类型',on_delete=models.SET_NULL, null=True, blank=True)
    parent = models.ForeignKey('self', on_delete=models.CASCADE, null=True, blank=True, related_name='children')
    # lesson = models.ForeignKey('CourseLesson', on_delete=models.SET_NULL, null=True, blank=True)
    # w = models.ManyToManyField('user.User')


    class Meta:
        db_table = 'tb_course_category'
        verbose_name = '课程名'
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.name


class CourseCategoryType(BaseModel):
    name = models.CharField('课程类', max_length=100, help_text='课程类',null=True)
    title = models.CharField('课程名', max_length=150, help_text='课程名', null=True)


    # course_pack = models.ForeignKey('CourseBag', on_delete=models.SET_NULL, null=True, blank=True)

    class Meta:
        db_table = 'tb_course_category_type'
        verbose_name = '课程类别'
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.name

class CourseLesson(BaseModel):
    name = models.CharField('课程章节', max_length=100, help_text='课程章节')
    category = models.ForeignKey('CourseCategory', on_delete=models.SET_NULL, null=True, blank=True)
    # course_pack = models.ForeignKey('CourseBag', on_delete=models.SET_NULL, null=True, blank=True)
    title = models.CharField('课程名', max_length=150, help_text='课程名', null=True)
    category_type = models.ForeignKey('CourseCategoryType', on_delete=models.SET_NULL, null=True, blank=True)


    class Meta:
        db_table = 'tb_course_lesson'
        verbose_name = '课程章节'
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.name




class Course(BaseModel):
    title = models.CharField('课程名', max_length=150, help_text='课程名')
    cover_url = models.URLField('封面url', help_text='封面url')
    video_url = models.URLField('课程视频url', help_text='课程视频url')
    duration = models.DurationField('课程时长', help_text='课程时长',null=True,blank=True)
    profile = models.TextField('课程简介', null=True, blank=True, help_text='课程简介')
    outline = models.TextField('课程大纲', null=True, blank=True, help_text='课程大纲')
    teacher = models.ForeignKey('Teacher', on_delete=models.SET_NULL, null=True, blank=True)
    category = models.ForeignKey('CourseCategory', on_delete=models.SET_NULL, null=True, blank=True)
    lesson = models.ForeignKey('CourseLesson', on_delete=models.SET_NULL,null=True,blank=True,related_name='children')
    is_pass = models.BooleanField('审核通过', default=False)
    is_example = models.BooleanField('是否为示范视频', default=False)
    is_outline = models.BooleanField('是否为教学大纲', default=False)
    category_type = models.ForeignKey('CourseCategoryType', on_delete=models.SET_NULL, null=True, blank=True)
    # w = models.ManyToManyField('User')



    class Meta:
        db_table = 'tb_course'
        verbose_name = '课程'
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.title
    # def __str__(self):

    #     return '%s,%s'%(self.category,self.lesson)
        # return self.title


class CourseBag(BaseModel):
    name = models.CharField('课件包名称', max_length=100, help_text='课件包名称')
    # category = models.ForeignKey('CourseCategory', on_delete=models.SET_NULL, null=True, blank=True)
    w = models.ManyToManyField('CourseCategory')
    title = models.CharField('课程名', max_length=150, help_text='课程名',null=True)

    class Meta:
        db_table = 'tb_course_bag'
        verbose_name = '课件包'
        verbose_name_plural = verbose_name

    def __str__(self):
        return '%s,%s'%(self.name,self.w)
