$(() => {
    $('.play').click(function () {
        let $this = $(this);
        console.log("haha");
        let aa = $this.attr('data-video-url');



        let player = cyberplayer("course-video").setup({
            width: '100%',
            height: '100%',
            file: aa,
            autostart: false,
            stretching: "uniform",
            repeat: false,
            volume: 100,
            controls: true,
            ak: '334025de9da34118b03f3666fc58232c'
        });
        $('#course-video').load(player);



        })  ;


    let $courseDel = $('.btn-del');      // 删除按钮
    menuId = 0;                                 // 被点击菜单id
    let $currentMenu = null;                    // 当前被点击菜单对象

    $courseDel.click(function () {
        let $this = $(this);
        $currentMenu = $this.parent().parent();
        courseId = $(this).data('id');
        let menuName = $this.data('name');

        // 改变模态框的显示内容
        $('#modal-delete .modal-body p').html('课程管理确定删除课程:《' + menuName + '》？');
        // 显示模态框
        $('#modal-delete').modal('show');
    });

    // 点击模特框确定删除按钮，发送ajax删除
    $('#modal-delete button.delete-confirm').click(() => {
        deleteMenu()
    });

    // 删除菜单的函数
    function deleteMenu() {
        $
            .ajax({
                url: '/myadmin/course/' + courseId + '/',
                type: 'DELETE',
                dataType: 'json'
            })
            .done((res) => {
                if (res.errno === '0') {
                    // 关闭模态框
                    $('#modal-delete').modal('hide');
                    // 删除菜单元素
                    $currentMenu.remove();
                    message.showSuccess(res.errmsg)
                }else if (res.errno === '4105') {
                    message.showError(res.errmsg)
                } else if(res.errno === '4101'){
                    message.showError(res.errmsg);
                    setTimeout(() => {
                        window.location.href = res.data.url
                    }, 1500)
                } else {
                    message.showError(res.errmsg)
                }
            })
            .fail(() => {
                message.showError('服务器超时，请重试！')
            })
    }


});