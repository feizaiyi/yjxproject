$(() => {

    let $unpassbtn = $('.un_pass');     // 删除按钮
    menuId = 0;                                 // 被点击菜单id
    let $currentMenu = null;                    // 当前被点击菜单对象

    $unpassbtn.click(function () {
        let $this = $(this);
        $currentMenu = $this.parent().parent();
        $unpassid = $(this).data('id');
        let menuName = $this.data('name');

        // 改变模态框的显示内容
        $('#modal-pass .modal-body p').html('课程管理确定tg课程:《' + menuName + '》？');
        // 显示模态框
        $('#modal-pass').modal('show');
    });

    // 点击模特框确定删除按钮，发送ajax删除
    $('#modal-pass button.delete-pass').click(() => {
        pass()
    });

    // 删除菜单的函数
    function pass() {
        $
            .ajax({
                url: '/myadmin/courses1/' + $unpassid + '/',
                type: 'PUT',
                dataType: 'json'
            })
            .done((res) => {
                if (res.errno === '0') {
                    // 关闭模态框
                    $('#modal-pass').modal('hide');
                    // 删除菜单元素
                    $currentMenu.remove();
                    message.showSuccess(res.errmsg)
                }else if (res.errno === '4105') {
                    message.showError(res.errmsg)
                } else if(res.errno === '4101'){
                    message.showError(res.errmsg);
                    setTimeout(() => {
                        window.location.href = res.data.url
                    }, 1500)
                    console.log(res.data.url)
                } else {
                    message.showError(res.errmsg)
                }
            })
            .fail(() => {
                message.showError('服务器超时，请重试！')
            })
    }


});
// / $(()=> {
//     let $unpassbtn = $('.un_pass');
//     $unpassbtn.click(function () {
//         console.log("准备审核了");
//         let passname = $(this).data('name');
//         let $unpassid = $(this).data('id');
//
//         $('#modal-delete .modal-body p').html('确定通过:《' + passname + '》？');
//         // 显示模态框
//         $('#modal-delete').modal('show');
//         // })
//
//         $('#modal-delete button.delete-confirm').click(() => {
//                 unPass()
//                 });
//             function unPass() {
//             $.ajax({
//                 url: '/myadmin/courses1/' + $unpassid + '/',
//                 type: 'PUT',
//                 dataType: 'json'
//             })
//                 .done((res) => {
//                     if (res.errno === '0') {
//                         // 关闭模态框
//                         $('#modal-delete').modal('hide');
//                         // 删除菜单元素
//                         // $currentMenu.remove();
//                         message.showSuccess('执行成功')
//                         $('#content').load(
//                             $('.sidebar-menu li.active a').data('url'),
//                             (response, status, xhr) => {
//                                 if (status !== 'success') {
//                                     message.showError('服务器超时，请重试！')
//                                 }
//                             }
//                         );
//                     }
//                 })
//                 .fail(() => {
//                     message.showError('服务器超时，请重试！')
//                 })
//         })
//
//
//     })
//
// })
