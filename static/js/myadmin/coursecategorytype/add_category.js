$(() => {
    let $addBtn = $('button.add');          //  模态框中的添加按钮
    let $haha = $('#add-menu');             //  模态矿中的表单

    $addBtn.click(() => {
        $
            .ajax({
                url: '/myadmin/coursecatelist/add/',
                type: 'POST',
                data: $haha.serialize()
            })
            .done((res) => {
                if (res.errno === '0') {
                    // 添加成功，关闭模态框，并刷新一下菜单列表
                    $('#modal-add').modal('hide').on('hidden.bs.modal', function (e) {
                        // 刷新一下菜单列表
                        $('#content').load(
                            $('.sidebar-menu li.active a').data('url'),
                            (response, status, xhr) => {
                                if (status !== 'success') {
                                    message.showError('服务器超时，请重试！')
                                }
                            }
                        )

                    })
                }else{
                    message.showError('添加菜单失败');
                    // 更新模态框中的表单信息
                    $('#modal-add .modal-content').html(res)
                }
            })
            .fail(()=>{
                message.showError('服务器超时，请重试！')
            })
    })

    let $updateBtn = $('#modal-update button.update');
    let $form = $('#update-menu');
    $updateBtn.click(function () {
        $
            .ajax({
                url: '/myadmin/menu/' + menuId + '/',
                type: 'PUT',
                data: $form.serialize(),
                // dataType: "json"
            })
            .done((res) => {
                if (res.errno === '0') {
                    $('#modal-update').modal('hide').on('hidden.bs.modal', function (e) {
                        $('#content').load(
                            $('.sidebar-menu li.active a').data('url'),
                            (response, status, xhr) => {
                                if (status !== 'success') {
                                    message.showError('服务器超时，请重试！')
                                }
                            }
                        );
                    });
                    message.showSuccess(res.errmsg);


                } else {
                    message.showError('修改菜单失败！');
                    $('#modal-update .modal-content').html(res)
                }
            })
            .fail(() => {
                message.showError('服务器超时，请重试');
            });
    });
})